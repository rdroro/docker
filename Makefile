base_root = ~/code/docker
cmd = cp $(base_root)

init-ionic:
	$(cmd)/ionic/* .

init-angular:
	$(cmd)/angular/* .

init-nodeje:
	$(cmd)/nodejs/* .
