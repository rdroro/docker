# Docker

Some useful Dockerfile and docker-compose MFB !

* [angular](./angular): `dev env` node:14-buster with `ng` and chromium installed 
* [ionic cordova](./ionic): `dev env` `not tested yet` complete env to dev ionic app with android local build with cordova
* [ionic capacitor](./ionic): `dev env` complete env to dev ionic app with android local build with capacitor 
